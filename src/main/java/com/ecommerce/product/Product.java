package com.ecommerce.product;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.stereotype.Component;

@MappedSuperclass
@Component
public class Product {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double price;
    private String name;
    private int ammount;


    public Product() {
    }


    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAmmount() {
        return ammount;
    }
    public void setAmmount(int ammount) {
        this.ammount = ammount;
    }


	@Override
	public String toString() {
		return "Product [id=" + id + ", price=" + price + ", name=" + name + ", ammount=" + ammount + "]";
	}


    

}
