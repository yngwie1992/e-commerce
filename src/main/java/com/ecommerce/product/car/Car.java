package com.ecommerce.product.car;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.ecommerce.product.Product;

@Entity
@Table(name = "car")
public class Car extends Product implements Serializable {


    private int vMAX;

    public Car() {
    }

    public int getvMAX() {
        return vMAX;
    }

    public void setvMAX(int vMAX) {
        this.vMAX = vMAX;
    }

	@Override
	public String toString() {
		return "Car [vMAX=" + vMAX + "]";
	}



}
