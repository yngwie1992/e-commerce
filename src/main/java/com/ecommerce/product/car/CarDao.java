package com.ecommerce.product.car;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ecommerce.dao.GenericService;
import com.ecommerce.user.User;

@Repository
public class CarDao implements GenericService {


    @Autowired
    private GenericCarRepository carRepository;

    @Override
    public List<Car> showAll(){
        return carRepository.findAll();
    }


    @Override
    public Car getById(Long id) {
        return carRepository.getOne(id);
    }

    @Override
    public void deleteById(Long id) {
    	carRepository.delete(carRepository.getOne(id));

    }


    @Override
    public void update(Object element) {

        //TODO
    }


    @Override
    public void addElement(Object element) {
    	carRepository.save((Car) element);

    }


	@Override
	public Object findByName(String name) {
		return carRepository.findByName(name);
	}
}
