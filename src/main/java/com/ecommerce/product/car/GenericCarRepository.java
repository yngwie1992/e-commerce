package com.ecommerce.product.car;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;


@Transactional
public interface GenericCarRepository extends JpaRepository<Car, Long> {

    Car findByName(String name);
}
