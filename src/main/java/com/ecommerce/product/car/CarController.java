package com.ecommerce.product.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CarController {

	
	@Autowired
	CarDao carDao;
	
	
	@GetMapping(value = "/cars")
	public String viewAll(Model model) {
		model.addAttribute("products", carDao.showAll());
		return "cars";
	}
	
	@PostMapping(value = "product/{id}")
	public String showProduct(Model model, @PathVariable long id) {
		model.addAttribute("product", carDao.getById(id));
		
		return "car";
	}
	
}
