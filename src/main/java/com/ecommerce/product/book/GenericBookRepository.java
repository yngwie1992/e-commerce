package com.ecommerce.product.book;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

@Transactional
public interface GenericBookRepository extends JpaRepository<Book, Long> {

    Book findByName(String name);
}
