package com.ecommerce.product.book;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecommerce.product.Product;

@Entity
@Table(name = "book")
public class Book extends Product {

	private int numberOfPages;
	
	public Book() {
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}
	
	
}
