package com.ecommerce.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.ecommerce.dao.GenericService;

@Service
public class UserDAO implements GenericService {


    @Autowired
    private GenericUserRepository userRepository;

    @Override
    public List<User> showAll(){
        return userRepository.findAll();
    }


    @Override
    public User getById(Long id) {
        return userRepository.getOne(id);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.delete(userRepository.getOne(id));

    }


    @Override
    public void update(Object element) {

        //TODO
    }


    @Override
    public void addElement(Object element) {
        userRepository.save((User) element);

    }
    
    public User getCurrentUser() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	System.out.println(auth.getName());
        return userRepository.findByUsername(auth.getName());
    }


	@Override
	public Object findByName(String name) {
		return userRepository.findByUsername(name);
	}
}
