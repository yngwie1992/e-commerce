package com.ecommerce.user;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.ecommerce.cart.Cart;


@Entity
@Component
@Table(name = "users")
public class User {

    @Id
    // TODO Read about generationType variations
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @Column(name = "USER_NAME")
    private String username;

//    @Column(name = "USER_PASSWORD")
    private String password;
    
//    @Column(name = "USER_ROLE")
    private String userRole;
    
//    @Column(name = "USER_ACTIVE")
    private boolean active;

//    @Column(name = "USER_AGE")
    private int age;
    
	@OneToOne(cascade=CascadeType.ALL)
	private Cart cart;
     
    
    public User() {}
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", userRole=" + userRole
				+ ", active=" + active + ", age=" + age + "]";
	}

	
	
    
}
