package com.ecommerce.user;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

@Transactional
public interface GenericUserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
