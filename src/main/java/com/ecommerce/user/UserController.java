package com.ecommerce.user;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {

		
	@GetMapping(value = "/shopHomePage")
	public String showHomePage() {
		return "shopHomePage";
	}
	
	@GetMapping(value = "/login")
	public String login() {
		return "login";
	}
	
//	@GetMapping(value = "/register")
//	public String register(Model model) {
//		model.addAttribute("user", new User());
//		
//		return "registerForm";
//	}
	
}
