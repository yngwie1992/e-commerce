package com.ecommerce.dao;

import java.util.List;

public interface GenericService<E> {

    List<?> showAll();

    E getById(Long id);

    void deleteById(Long id);

    void update(E element);

    void addElement(E element);
    
    E findByName(String name);



}
