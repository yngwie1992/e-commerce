package com.ecommerce.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
//	   @Autowired
//	   UserDetailsServiceImpl userDetailsService;
	

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/register", "/h2-console/**").permitAll().anyRequest()
				.authenticated().and().formLogin().loginPage("/login").permitAll().and().logout().logoutSuccessUrl("/")
				.permitAll();

		http.csrf().disable();
		http.headers().frameOptions().disable();

	}

	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		UserDetails user = User.withDefaultPasswordEncoder().username("1").password("1").roles("USER").build();

		return new InMemoryUserDetailsManager(user);
	}
	
//	   @Bean
//	   public BCryptPasswordEncoder passwordEncoder() {
//	      BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//	      return bCryptPasswordEncoder;
//	   }
//	   
//	   @Autowired
//	   public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//	 
//	      // Setting Service to find User in the database.
//	      // And Setting PassswordEncoder
//	      auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
//	 
//	   }
}
