//package com.ecommerce.security;
// 
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import com.ecommerce.entity.User;
//import com.ecommerce.user.UserDAO;
// 
//@Service
//public class UserDetailsServiceImpl implements UserDetailsService {
// 
//    @Autowired
//    private UserDAO UserDAO;
// 
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        User user = (User) UserDAO.findByName(username);
//        System.out.println("User= " + user);
// 
//        if (user == null) {
//            throw new UsernameNotFoundException("User " //
//                    + username + " was not found in the database");
//        }
// 
//        // EMPLOYEE,MANAGER,..
//        String role = user.getUserRole();
// 
//        List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
// 
//        // ROLE_EMPLOYEE, ROLE_MANAGER
//        GrantedAuthority authority = new SimpleGrantedAuthority(role);
// 
//        grantList.add(authority);
// 
//        boolean enabled = user.isActive();
//        boolean accountNonExpired = true;
//        boolean credentialsNonExpired = true;
//        boolean accountNonLocked = true;
//       
//        UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User(user.getUsername(), //
//                user.getPassword(), enabled, accountNonExpired, //
//                credentialsNonExpired, accountNonLocked, grantList);
// 
//        return userDetails;
//    }
// 
//}