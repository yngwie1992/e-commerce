package com.ecommerce.cart;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

@Transactional
public interface GenericCartRepository extends JpaRepository<Cart, Long> {

}
