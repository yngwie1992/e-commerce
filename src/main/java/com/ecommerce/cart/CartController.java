package com.ecommerce.cart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.ecommerce.product.car.CarDao;
import com.ecommerce.user.UserDAO;

@Controller
public class CartController {
	
	@Autowired
	CarDao carDao;
	
	@Autowired
	UserDAO userDao;
	
	
	
	@GetMapping(value = "shopHomePage/cart")
	public String showCart(Model model) {
		
		model.addAttribute("products", userDao.getCurrentUser().getProducts());
		
		return "cart";
	}
	
	@GetMapping(value = "/cart/add/{id}")
	public String addToCart(@PathVariable Long id) {
		
		System.out.println(userDao.getCurrentUser().toString());
		System.out.println(carDao.getById(id).toString());
		
		userDao.getCurrentUser().setProductToProductList(carDao.getById(id));
		
		return "redirect:/shopHomePage/cart";
	}
	
}
