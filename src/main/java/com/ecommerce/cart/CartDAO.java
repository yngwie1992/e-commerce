package com.ecommerce.cart;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.ecommerce.dao.GenericService;
import com.ecommerce.product.Product;

@Repository
public class CartDAO {

	@Autowired
	private GenericCartRepository cartRepository;
	
	
	public List<Product> getProductsFromCard(){
		List<Product> products = new ArrayList<>();
		for(Cart cart : cartRepository.findAll()) {
			products.add(cart.getProduct());
		}
		return products;
	}
	
	public void addProductToCard(Product product) {
		Cart cart = new Cart();
		cart.setProduct(product);
		cartRepository.save(cart);
	}
	
	public void deteleProductFromCart(Product product) {
		List<Product> products = new ArrayList<>();
		for(Cart cart : cartRepository.findAll()) {
			if(cart.getProduct().getName() == product.getName()) {
				cartRepository.delete(cart);
			}
		}
	}
	
}
